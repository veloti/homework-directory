//Функция конструктор для создания объекта человека
function Human(name, age, job = 'jobless', childrens = 0) {
	this.name = name;
	this.age = age;
	this.job = job;
	this.childrens = childrens
}


const alex = new Human('Alex', 20);
const daniel = new Human('Daniel', 44, 'dantist');
const sophie = new Human('Sophie', 44, 'dantist', 1);
const tom = new Human('Tom', 44, 'dantist', 3);

Human.prototype.hi = function() {
	alert(`Hi, my name is ${this.name} and I'm a human!`)
}
Human.prototype.do = function() {
	alert(`What I'm good at? I'm a ${this.job}.`);
}
Human.prototype.family = function() {
	if (this.childrens == 0) return alert("I have no children");
	alert(`I have ${this.childrens} ${(this.childrens==1?'child':'children')}`);
}
Human.prototype.introduce = function() {
	this.hi();
	this.do();
	this.family();
}


alex.hi();
alex.do();
daniel.do();
sophie.family();
tom.family();
tom.introduce();

