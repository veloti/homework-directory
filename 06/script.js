//Функция конструктор для создания объекта человека
function Human(name, age) {
	this.name = name;
	this.age = age;
}

//Объявляем пустой массив и заполняем его объектами Human
const humanArray = [];

humanArray.push(new Human('Chad', 22));
humanArray.push(new Human('Alex', 18));
humanArray.push(new Human('Diana', 21));
humanArray.push(new Human('Alice', 17));
humanArray.push(new Human('Bryan', 40));
humanArray.push(new Human('Tom', 22));

//Сортируем массив по возрасту
humanArray.sort((a,b) => {
	if (a.age > b.age) return 1;
	if (a.age == b.age ) return 0;
	if (a.age < b.age) return -1;
})

//Выводим на экран
for(elem of humanArray) {
	document.write(`name: ${elem.name} --- age: ${elem.age} <hr/>`)
}

