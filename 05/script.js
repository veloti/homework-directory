const document1 = {
	header: null,
	body: null,
	footer: null,
	data: null,
	app: {
		name: 'test',
		headerAdd(props) {
			document1.header = props;
		},
		bodyAdd(props222) {
			document1.body = props222;
		},
		footerAdd(props) {
			document1.footer = props;
		},
		dataAdd(props) {
			document1.data = props
		},
		showDoc() {
			document.write(document1.header);
			document.write('<hr>');
			document.write(document1.body);
			document.write('<hr>');
			document.write(document1.footer);
			document.write('<hr>');
			document.write(document1.data);
		}
	}
}

//Заполняем header

document1.app.headerAdd('<h1>Hello world</h1>');

//Заполняем body

document1.app.bodyAdd("<img src='https://avatanplus.com/files/resources/mid/5dd9360db3e1f16e987b25a9.png'>");

//Заполняем footer

document1.app.footerAdd('<p>Test</p>');

//Заполняем data 

document1.app.dataAdd('321321312321321312');

//Выводим документ
document1.app.showDoc();