function filterBy(array, datatype) {
	const newArray = [];
	for(elem of array) {
		if(elem === null && datatype == 'null') continue;
		if(typeof(elem) == datatype) continue;
		newArray.push(elem)
	}
	return newArray;
}

let testArray = ['hello','world', 23, '23', null];
console.log(filterBy(testArray, 'string'));