//Функция которая создает обьект человека и возвращает его
function createNewUser() {
	const firstName = prompt('Введите имя');
	const secondName = prompt('Введите фамилию');
	const birthday = prompt('Введите дату рождения в формате dd.mm.yyyy')

	const newUser = {
		firstName,
		secondName,
		birthday,
		getLogin() {
			return (this.firstName[0]+this.secondName).toLowerCase()
		},
		getAge() {
			return (new Date()).getFullYear() - birthday.split('.')[2]
		},
		getPassword() {
			return this.firstName[0].toUpperCase() + this.secondName.toLowerCase() + birthday.split('.')[2]
		}

	}

	return newUser
}

const newUser = createNewUser();

console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());