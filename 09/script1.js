const input = document.querySelector('#input');
const button = document.querySelector('#button');
const number = Math.floor(Math.random()*100);
const output = document.querySelector('#output');

function checkNumber() {
	let outputText,
		value = input.value;

	if (value > number) {
		outputText = 'Загаданное значение меньше введеного вами';
	} else if (value < number) {
		outputText = 'Загаданное значение больше введеного вами';
	} else {
		outputText = 'Вы угадали!';
	}

	output.innerHTML = outputText;
}

button.addEventListener('click', checkNumber)