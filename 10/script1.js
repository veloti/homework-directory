const state = {
    first: "",
    second: "",
    func: "",
}

const output = document.querySelector(".output");

const clear = document.querySelector("#clear");

const result = document.querySelector("#result")

const numbers = document.querySelectorAll(".number");

const operations = document.querySelectorAll(".operation");

const percent = document.querySelector((".percent"));

//-------------------------------------------------------------------

clear.addEventListener("click", () => {
    clearFunction();
})

result.addEventListener("click", () => {
    if (state.first && state.second && state.func) {
        let resultNumber = calculate(+state.first, +state.second, state.func);
        clearFunction();
        state.first = resultNumber;
        updateOutput();
    }
})

for(let elem of operations) {
    if (elem.innerHTML == "-") {
        elem.addEventListener("click", () => {
            if (state.first) {
                state.func = elem.innerHTML;
                updateOutput();

            } else {
                state.first += elem.innerHTML;
                updateOutput();
            }

        })
        continue;
    }

    if (elem.innerHTML == "+/-") {
        elem.addEventListener("click",() => {
            if (state.first[0] == "-" && state.first.length) {
                state.first = state.first.slice(1)
                updateOutput();
            } else if (state.first[0] && state.first.length) {
                state.first = "-" + state.first
                updateOutput();
            }
        })
        continue;
    }
    elem.addEventListener("click", () => {
        state.func = elem.innerHTML;
        updateOutput();
    })
}



for(let elem of numbers) {
    elem.addEventListener("click", () => {
        if (state.first && state.func) {
            state.second += elem.innerHTML;
            updateOutput();
        } else {
            state.first += elem.innerHTML;
            updateOutput();
        }
    })
}

const calculate = (a,b,f) => {
    switch (f) {
        case "+":
            return String(a+b);
        case "-":
            return String(a-b);
        case "x":
            return String(a*b);
        case "/":
            return String(a/b);
        case "%":
            return String((a/100)*b);

    }
}

const clearFunction = () => {
    state.first = "";
    state.second = "";
    state.func = "";
    updateOutput();
}

const updateOutput = () => {
    let outputString = `${state.first} ${state.func} ${state.second}`;
    output.innerHTML = outputString;
}