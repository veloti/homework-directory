let arrayFirst = [1,2,3,4,5];

function map(fn, array) {
	return array.map(item => fn(item));
}

function fnFirst(item) {
	return item*2
}

function checkAge(age) {
	return (age > 18 || confirm("Родители разрешили?"))
}

//вызываем функцию, которая умножает число на два с массивом [1,2,3,4,5]
alert(map(fnFirst,arrayFirst));